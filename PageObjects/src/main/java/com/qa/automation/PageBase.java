package com.qa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

public class PageBase {
    private WebDriver webDriver;

    public PageBase(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private WebElement getElement(String xPath) {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 20);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        WebElement webElement = webDriver.findElement(By.xpath(xPath));

        return webElement;
    }

    protected void inputText(String xPath, String inputValue) {
        WebElement webElement = getElement(xPath);
        webElement.sendKeys(inputValue);
    }

    protected void clickControl(String xPath) {
        WebElement webElement = getElement(xPath);
        webElement.click();
    }

    protected void selectDropDownItem(String xPath, String itemName) {
        WebElement webElement = getElement(xPath);
        Select select = new Select(webElement);
        select.selectByVisibleText(itemName);
    }

    protected void selectDropDownItem(String xPath, int itemName) {
        WebElement webElement = getElement(xPath);
        Select select = new Select(webElement);
        select.selectByVisibleText(Integer.toString(itemName));
    }

    public void logger(String log) {
        System.out.println(log);
    }
}

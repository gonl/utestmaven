package com.qa.automation.PageObjectsAction;

import com.qa.automation.PageObjects.PersonalPage;
import org.openqa.selenium.WebDriver;

public class PersonalPageActions extends PersonalPage {


    public PersonalPageActions(WebDriver webDriver) {
        super(webDriver);
    }

    public void inputFirstName(String firstName) {
        logger("input first name - " + firstName);
        inputFirstNameTextBox(firstName);
    }

    public void inputLastName(String lastName) {
        logger("input last name - " + lastName);
        inputLastNameTextBox(lastName);
    }

    public void inputEmail(String email) {
        logger("input email" + email);
        inputEmailTextBox(email);
    }

    public void selectBirthDay (String month, int day, int year) {
        logger("select birthday - " + month + " - " + day + " - " + year);
        selectMonth(month);
        selectDay(day);
        selectYear(year);
    }

    public void clickOnNextButton() {
        logger("click on next button");
        clickOnNextButtonControl();
    }

    private void selectMonth(String month) {
        logger("select month - " + month);
        selectBirthMonthDropDown(month);
    }

    private void selectDay(int day) {
        logger("select day - " + day);
        selectBirthDayDropDown(day);
    }

    private void selectYear(int year) {
        logger("select year - " + year);
        selectBirthYearDropDown(year);
    }
}

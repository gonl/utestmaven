package com.qa.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestCaseBase {
    public WebDriver driver;
    //public RemoteWebDriver driver;

    @BeforeSuite
    public void init() throws MalformedURLException {
        String userDir = System.getProperty("user.dir");
        if(System.getProperty("os.name").contains("Windows")) {
            System.setProperty("webdriver.chrome.driver", userDir + "\\chromedriver.exe");
        } else {
            System.setProperty("webdriver.chrome.driver", userDir + "/chromedriver_linux");
        }
        
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

        driver = new ChromeDriver(options);
    }

    public void browseUrl(String url) {
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }

    public WebDriver getDriver() {
        return driver;
    }

    @AfterClass
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}

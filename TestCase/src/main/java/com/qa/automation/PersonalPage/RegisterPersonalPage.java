package com.qa.automation.PersonalPage;

import com.qa.automation.PageObjectsAction.PersonalPageActions;
import com.qa.automation.TestCaseBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RegisterPersonalPage extends TestCaseBase {
    private static final String PERSONAL_PAGE_URL = "https://www.utest.com/signup/personal";
    public PersonalPageActions personalPageActions;


    @BeforeClass
    public void setup() {
        personalPageActions = new PersonalPageActions(getDriver());
    }

    @Test
    public void navigateToPersonalPage() {
        browseUrl(PERSONAL_PAGE_URL);
    }

    @Test(dependsOnMethods = {"navigateToPersonalPage"})
    public void inputFirstName() {
        personalPageActions.inputFirstName("Long");
    }

    @Test(dependsOnMethods = {"inputFirstName"})
    public void inputLastName() {
        personalPageActions.inputLastName("Nguyen Bao");
    }

    @Test(dependsOnMethods = {"inputLastName"})
    public void inputEmail() {
        personalPageActions.inputEmail("long.nguyen@utest.com");
    }

    @Test(dependsOnMethods = {"inputEmail"})
    public void selectBirthDay() {
        personalPageActions.selectBirthDay("February", 24, 1988);
    }

    @Test(dependsOnMethods = {"selectBirthDay"})
    public void clickNextButton() {
        //personalPageActions.clickOnNextButton();
    }




}
